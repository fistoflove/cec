<?php

use Timber\Timber;


require_once __DIR__ . '/vendor/autoload.php';

\Sentry\init(['dsn' => 'https://dec46aa84e5144a1a59fcdf8991ef0a1@o4505186700754944.ingest.sentry.io/4505192041742336' ]);

// Register custom post types
require_once('register-custom-post-types.php');

// Register custom taxonomies
require_once('register-custom-taxonomies.php');

function acf_filter_rest_api_preload_paths( $preload_paths ) {
    if ( ! get_the_ID() ) {
      return $preload_paths;
    }
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '?context=edit';
    $v1 =  array_filter(
      $preload_paths,
      function( $url ) use ( $remove_path ) {
        return $url !== $remove_path;
      }
    );
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '/autosaves?context=edit';
    return array_filter(
      $v1,
      function( $url ) use ( $remove_path ) {
        return $url !== $remove_path;
      }
    );
  }
  add_filter( 'block_editor_rest_api_preload_paths', 'acf_filter_rest_api_preload_paths', 10, 1 );


add_action( 'wp_ajax_nopriv_get_course_subcategories', 'get_course_subcategories' );

add_action( 'wp_ajax_get_course_subcategories', 'get_course_subcategories' );
  
  
function get_course_subcategories() {

  $context = Timber::get_context();
  $context['subcategories'] = get_terms( array(
    'taxonomy' => 'course-category',
    'hide_empty' => false,
    'child_of' => $_POST['category'],
    'meta_key'  => 'course_finder_priority',
    'orderby'   => 'meta_value_num',
    'order' => 'DESC',
  ));
  if($_POST['fancy'] == "true") {
    Timber::render( 'select-option-fancy.twig', $context );
  } else {
    Timber::render( 'select-option.twig', $context );
  }
  die();
}

function remove_menus(){
  remove_menu_page( 'edit-comments.php' );          //Comments  

}

if ( current_user_can( 'editor' ) ){
    add_action( 'admin_menu', 'remove_menus' );
}
/**
 * Disable the emoji's
 */
function disable_emojis() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
  add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
 }

 add_action( 'init', 'disable_emojis' );

 /**
  * Filter function used to remove the tinymce emoji plugin.
  * 
  * @param array $plugins 
  * @return array Difference betwen the two arrays
  */

 function disable_emojis_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
  return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
  return array();
  }
 }
 
 /**
  * Remove emoji CDN hostname from DNS prefetching hints.
  *
  * @param array $urls URLs to print for resource hints.
  * @param string $relation_type The relation type the URLs are printed for.
  * @return array Difference betwen the two arrays.
  */
 function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
  if ( 'dns-prefetch' == $relation_type ) {
  /** This filter is documented in wp-includes/formatting.php */
  $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
 
 $urls = array_diff( $urls, array( $emoji_svg_url ) );
  }
 
 return $urls;
 }

 $cats = wp_get_post_categories( get_the_ID() );  //post id
 foreach($cats as $c) {
     $ancestors = array_merge( array_reverse( get_ancestors( $c, 'category' ) ), [$c] );
     echo '<ul class="post-categories">';
     foreach($ancestors as $id){
         echo '<li><a href="' . esc_url( get_category_link( $id) ) . '">' . get_cat_name( $id ) . '</a></li>';
     }
     echo '</ul>';
 }


add_filter( 'timber/twig', 'add_to_twig_theme' );

function add_to_twig_theme( $twig ) {
  // Adding a function.
  $twig->addFunction( new Twig_Function( '_get_course_categories', '_get_course_categories' ) );
  $twig->addFunction( new Twig_Function( '_get_course_color', '_get_course_color' ) );
  return $twig;
}
 
function _get_course_categories( $course_id ) {
  $items = get_the_terms($course_id, 'course-category');
  return $items;
}

function _get_course_color( $course_id, $term='course-category', $return_all_categories=false ) {

  if (empty($return['primary_category']) || $return_all_categories){
    $categories_list = get_the_terms($course_id, $term);

    if (empty($return['primary_category']) && !empty($categories_list)){
        $return['primary_category'] = $categories_list[0];
    }
    if ($return_all_categories){
        $return['all_categories'] = array();

        if (!empty($categories_list)){
            foreach($categories_list as &$category){
                $return['all_categories'][] = $category->term_id;
            }
        }
    }
    $color = get_field('category_colour', $return['primary_category']);
    if ($color) {
      return $color['color'];
    } else {
      return "#BE1E2D";
    }
  }
}

if(!is_admin() && $GLOBALS["pagenow"] != "wp-login.php"){
	function unload_um_styles(){

		// Deregister & dequeue styles
		wp_dequeue_style(array(
			"um_fonticons_ii",
			"um_fonticons_fa",
			"select2",
			"um_modal",
			"um_styles",
			"um_members",
			"um_profile",
			"um_account",
			"um_misc",
			"um_fileupload",
			"um_datetime",
			"um_datetime_date",
			"um_datetime_time",
			"um_raty",
			"um_scrollbar",
			"um_crop",
			"um_tipsy",
			"um_responsive",
			"um_default_css",
		));

		wp_deregister_style(array(
			"um_fonticons_ii",
			"um_fonticons_fa",
			"select2",
			"um_modal",
			"um_styles",
			"um_members",
			"um_profile",
			"um_account",
			"um_misc",
			"um_fileupload",
			"um_datetime",
			"um_datetime_date",
			"um_datetime_time",
			"um_raty",
			"um_scrollbar",
			"um_crop",
			"um_tipsy",
			"um_responsive",
			"um_default_css",
		));

	};
	add_action("wp_print_styles","unload_um_styles");

};

add_filter('get_post_status', function($post_status, $post) {
  if ($post->post_type == 'activity' && $post_status == 'future') {
      return "publish";
  }
  return $post_status;
}, 10, 2);

?>

