<?php
if ( ! function_exists('Case_Study_post_type') ) {

// Register Custom Post Type
function Course_post_type() {

	$labels = array(
		'name'                  => _x( 'Courses', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Course', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Courses', 'ims_theme' ),
		'name_admin_bar'        => __( 'Course', 'ims_theme' ),
		'archives'              => __( 'Course Archives', 'ims_theme' ),
		'attributes'            => __( 'Course Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Courses', 'ims_theme' ),
		'add_new_item'          => __( 'Add Course', 'ims_theme' ),
		'add_new'               => __( 'Add Course', 'ims_theme' ),
		'new_item'              => __( 'New Course', 'ims_theme' ),
		'edit_item'             => __( 'Edit Course', 'ims_theme' ),
		'update_item'           => __( 'Update Course', 'ims_theme' ),
		'view_item'             => __( 'View Course', 'ims_theme' ),
		'view_items'            => __( 'View Courses', 'ims_theme' ),
		'search_items'          => __( 'Search Course', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Course', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Course', 'ims_theme' ),
		'items_list'            => __( 'Courses list', 'ims_theme' ),
		'items_list_navigation' => __( 'Courses list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter Courses list', 'ims_theme' ),
	);
	$rewrite = array(
		'slug'                  => '',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Course', 'ims_theme' ),
		'description'           => __( 'Courses', 'ims_theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 23,
		'menu_icon'             => get_stylesheet_directory_uri() . '/static/icons/graduation-cap.svg',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => '',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		// 'taxonomies'  => array( 'post_tag' ),
	);
	register_post_type( 'Course', $args );

}

add_action( 'init', 'Course_post_type', 0 );

function Testimonial_post_type() {

	$labels = array(
		'name'                  => _x( 'Student Testimonials', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Testimonials', 'ims_theme' ),
		'name_admin_bar'        => __( 'Testimonial', 'ims_theme' ),
		'archives'              => __( 'Testimonial Archives', 'ims_theme' ),
		'attributes'            => __( 'Testimonial Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Testimonials', 'ims_theme' ),
		'add_new_item'          => __( 'Add Testimonial', 'ims_theme' ),
		'add_new'               => __( 'Add Testimonial', 'ims_theme' ),
		'new_item'              => __( 'New Testimonial', 'ims_theme' ),
		'edit_item'             => __( 'Edit Testimonial', 'ims_theme' ),
		'update_item'           => __( 'Update Testimonial', 'ims_theme' ),
		'view_item'             => __( 'View Testimonial', 'ims_theme' ),
		'view_items'            => __( 'View Testimonials', 'ims_theme' ),
		'search_items'          => __( 'Search Testimonial', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Testimonial', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'ims_theme' ),
		'items_list'            => __( 'Testimonials list', 'ims_theme' ),
		'items_list_navigation' => __( 'Testimonials list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter Testimonials list', 'ims_theme' ),
	);
	$rewrite = array(
		'slug'                  => '',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'ims_theme' ),
		'description'           => __( 'Student Testimonials', 'ims_theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 24,
		'menu_icon'             => 'dashicons-visibility',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => false,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		// 'taxonomies'  => array( 'post_tag' ),
	);
	register_post_type( 'Testimonial', $args );

}

add_action( 'init', 'Testimonial_post_type', 0 );

function Activity_post_type() {

	$labels = array(
		'name'                  => _x( 'Activities', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Activity', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Activities', 'ims_theme' ),
		'name_admin_bar'        => __( 'Activity', 'ims_theme' ),
		'archives'              => __( 'Activity Archives', 'ims_theme' ),
		'attributes'            => __( 'Activity Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Activities', 'ims_theme' ),
		'add_new_item'          => __( 'Add Activity', 'ims_theme' ),
		'add_new'               => __( 'Add Activity', 'ims_theme' ),
		'new_item'              => __( 'New Activity', 'ims_theme' ),
		'edit_item'             => __( 'Edit Activity', 'ims_theme' ),
		'update_item'           => __( 'Update Activity', 'ims_theme' ),
		'view_item'             => __( 'View Activity', 'ims_theme' ),
		'view_items'            => __( 'View Activities', 'ims_theme' ),
		'search_items'          => __( 'Search Activity', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Activity', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Activity', 'ims_theme' ),
		'items_list'            => __( 'Activities list', 'ims_theme' ),
		'items_list_navigation' => __( 'Activities list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter Activities list', 'ims_theme' ),
	);
	$rewrite = array(
		'slug'                  => 'activities',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Activity', 'ims_theme' ),
		'description'           => __( 'Activities', 'ims_theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 25,
		'menu_icon'             => 'dashicons-buddicons-activity',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		// 'taxonomies'  => array( 'post_tag' ),
	);
	register_post_type( 'Activity', $args );

}

add_action( 'init', 'Activity_post_type', 0 );

function Accommodation_post_type() {

	$labels = array(
		'name'                  => _x( 'Accommodations', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Accommodation', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Accommodations', 'ims_theme' ),
		'name_admin_bar'        => __( 'Accommodation', 'ims_theme' ),
		'archives'              => __( 'Accommodation Archives', 'ims_theme' ),
		'attributes'            => __( 'Accommodation Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Accommodations', 'ims_theme' ),
		'add_new_item'          => __( 'Add Accommodation', 'ims_theme' ),
		'add_new'               => __( 'Add Accommodation', 'ims_theme' ),
		'new_item'              => __( 'New Accommodation', 'ims_theme' ),
		'edit_item'             => __( 'Edit Accommodation', 'ims_theme' ),
		'update_item'           => __( 'Update Accommodation', 'ims_theme' ),
		'view_item'             => __( 'View Accommodation', 'ims_theme' ),
		'view_items'            => __( 'View Accommodations', 'ims_theme' ),
		'search_items'          => __( 'Search Accommodation', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Accommodation', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Accommodation', 'ims_theme' ),
		'items_list'            => __( 'Accommodations list', 'ims_theme' ),
		'items_list_navigation' => __( 'Accommodations list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter Accommodations list', 'ims_theme' ),
	);
	$rewrite = array(
		'slug'                  => 'accommodation',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Accommodation', 'ims_theme' ),
		'description'           => __( 'Accommodations', 'ims_theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 26,
		'menu_icon'             => 'dashicons-building',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		// 'taxonomies'  => array( 'post_tag' ),
	);
	register_post_type( 'Accommodation', $args );

}

add_action( 'init', 'Accommodation_post_type', 0 );

function Festival_post_type() {

	$labels = array(
		'name'                  => _x( 'Festivals', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Festival', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Festivals', 'ims_theme' ),
		'name_admin_bar'        => __( 'Festival', 'ims_theme' ),
		'archives'              => __( 'Festival Archives', 'ims_theme' ),
		'attributes'            => __( 'Festival Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Festivals', 'ims_theme' ),
		'add_new_item'          => __( 'Add Festival', 'ims_theme' ),
		'add_new'               => __( 'Add Festival', 'ims_theme' ),
		'new_item'              => __( 'New Festival', 'ims_theme' ),
		'edit_item'             => __( 'Edit Festival', 'ims_theme' ),
		'update_item'           => __( 'Update Festival', 'ims_theme' ),
		'view_item'             => __( 'View Festival', 'ims_theme' ),
		'view_items'            => __( 'View Festivals', 'ims_theme' ),
		'search_items'          => __( 'Search Festival', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Festival', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Festival', 'ims_theme' ),
		'items_list'            => __( 'Festivals list', 'ims_theme' ),
		'items_list_navigation' => __( 'Festivals list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter Festivals list', 'ims_theme' ),
	);
	$rewrite = array(
		'slug'                  => 'festivals',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Festival', 'ims_theme' ),
		'description'           => __( 'Festivals', 'ims_theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 25,
		'menu_icon'             => 'dashicons-tickets',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		// 'taxonomies'  => array( 'post_tag' ),
	);
	register_post_type( 'Festival', $args );

}

add_action( 'init', 'Festival_post_type', 0 );
}