<?php
// Enter your code here, enjoy!
$array = [
  [
    "duration" => "4",
    "price" => "250"
  ],
  [
    "duration" => "8",
    "price" => "200"
  ],
  [
    "duration" => "12",
    "price" => "175"
  ]
];

$duration = 12;

$price = 0;

foreach( $array as $key => $value ){
	if(intval($value['duration']) == $duration) {
		$price = $value['duration'] * $value['price'];
	}
}

echo $price;
