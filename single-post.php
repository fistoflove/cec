<?php

use Timber\Timber;
use Timber\Post;

$context = Timber::get_context();

$timber_post = new Post();

$context['post'] = $timber_post;

$context['fields'] = get_fields();

$args = [
    'post_type' => 'post',
    'posts_per_page' => 5,
    'order' => 'ASC',
	'post__not_in' => array($timber_post->id),
];
$context['activities'] = Timber::get_posts( $args );

Timber::render( 'single-post.twig', $context );

