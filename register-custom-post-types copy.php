<?php

function Festival_post_type() {

	$labels = array(
		'name'                  => _x( 'Festivals', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Festival', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Festivals', 'ims_theme' ),
		'name_admin_bar'        => __( 'Festival', 'ims_theme' ),
		'archives'              => __( 'Festival Archives', 'ims_theme' ),
		'attributes'            => __( 'Festival Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Festivals', 'ims_theme' ),
		'add_new_item'          => __( 'Add Festival', 'ims_theme' ),
		'add_new'               => __( 'Add Festival', 'ims_theme' ),
		'new_item'              => __( 'New Festival', 'ims_theme' ),
		'edit_item'             => __( 'Edit Festival', 'ims_theme' ),
		'update_item'           => __( 'Update Festival', 'ims_theme' ),
		'view_item'             => __( 'View Festival', 'ims_theme' ),
		'view_items'            => __( 'View Festivals', 'ims_theme' ),
		'search_items'          => __( 'Search Festival', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Festival', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Festival', 'ims_theme' ),
		'items_list'            => __( 'Festivals list', 'ims_theme' ),
		'items_list_navigation' => __( 'Festivals list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter Festivals list', 'ims_theme' ),
	);
	$rewrite = array(
		'slug'                  => '',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Festival', 'ims_theme' ),
		'description'           => __( 'Festivals', 'ims_theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 25,
		'menu_icon'             => 'dashicons-tickets',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => false,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		// 'taxonomies'  => array( 'post_tag' ),
	);
	register_post_type( 'Festival', $args );

}

add_action( 'init', 'Festival_post_type', 0 );
