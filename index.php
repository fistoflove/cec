<?php

use Timber\Timber;
use Timber\PostQuery;

$context = Timber::get_context();

if (is_404()) {
    Timber::render( '404.twig', $context );
} else {
    if(get_post_type() == "post") {
        $args = [
            'post_type' => 'post',
            'order' => 'DESC',
            'orderby' => 'date'
        ];
        
        $context['posts'] = Timber::get_posts( $args );
    }
    Timber::render( 'archive.twig', $context );
}

?>
