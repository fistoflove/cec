<?php

use Timber\Timber;
use Timber\Post;

$context = Timber::get_context();

$timber_post = new Post();

$context['post'] = $timber_post;

$context['fields'] = get_fields();

$args = [
    'post_type' => 'activity',
    'posts_per_page' => 5,
    'order' => 'ASC',
    'orderby' => 'date',
	'post__not_in' => array($timber_post->id),
    'post_status' => array('future')
];
$context['activities'] = Timber::get_posts( $args );

Timber::render( 'single-activity.twig', $context );

