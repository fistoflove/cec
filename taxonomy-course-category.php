<?php

use Timber\Timber;
use Timber\PostQuery;

$context = Timber::get_context();

$term = get_queried_object();

$context['fields'] = get_fields();

$context['term'] = get_queried_object();

$context['page_title'] = get_field('page_title', $context['term']);

$context['primary_content'] = get_field('primary_content', $context['term']);

$context['category_image'] = get_field('category_image', $context['term']);

$context['background_image'] = get_field('background_image', $context['term']);

$context['category_colour'] = get_field('category_colour', $context['term']);

$context['display_mode'] = get_field('display_mode', $context['term']);

$context['feed_type'] = get_field('feed_type', $context['term']);

$args = [
    'post_type' => 'testimonial',
    'posts_per_page' => 3,
    'order' => 'ASC'
];

$context['testimonials'] = Timber::get_posts( $args );

if($context['feed_type'] == 'subcategories') {

    $context['items'] = Timber::get_terms([
        'taxonomy'  => 'course-category',
        'hide_empty'    => false,
        'child_of'  => $context['term']->term_id,
        'meta_key'  => 'priority',
        'orderby'   => 'meta_value_num',
        'order' => 'DESC'
    ]);

} elseif($context['feed_type'] == 'courses') {
    $args = [
        'post_type' => 'course',
        'posts_per_page' => 12,
        'tax_query' => array(
            array(
                'taxonomy' => 'course-category',
                'field' => 'id',
                'terms' => $context['term']->term_id
            )
        )
    ];
    $context['items'] = Timber::get_posts( $args );
}

if (is_404()) {

    Timber::render( '404.twig', $context );

} else {
    if($context['display_mode'] == 'primary') {

        Timber::render( 'taxonomy-course-category-one.twig', $context );

    } elseif($context['display_mode'] == 'secondary') {

        Timber::render( 'taxonomy-course-category-two.twig', $context );

    }
}


// $flexible_content = get_field('flexible_content', $context['term']);

// foreach ($flexible_content as $content) {
//     $data = $content['data'];
//     include $content['acf_fc_layout'];
// }

?>