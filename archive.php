<?php

use Timber\Timber;
use Timber\Post;

$templates = array( 'archive.twig', 'index.twig' );

$context = Timber::get_context();

$context['title'] = 'Archive';

if ( is_day() ) {

	$context['title'] = 'Archive: ' . get_the_date( 'D M Y' );

} elseif ( is_month() ) {

	$context['title'] = 'Archive: ' . get_the_date( 'M Y' );

} elseif ( is_year() ) {

	$context['title'] = 'Archive: ' . get_the_date( 'Y' );

} elseif ( is_tag() ) {

	$context['title'] = single_tag_title( '', false );

} elseif ( is_category() ) {

	$context['title'] = single_cat_title( '', false );

	array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );

} elseif ( is_post_type_archive() ) {

	$context['title'] = post_type_archive_title( '', false );

	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
}

if(get_post_type() == "activity") {
    $args = [
        'post_type' => 'activity',
		'order' => 'ASC',
		'orderby' => 'date',
		'posts_per_page' => '-1',
        'post_status' => array('future')
    ];
    
	$context['future_posts'] = Timber::get_posts( $args );

    $args = [
        'post_type' => 'activity',
		'order' => 'ASC',
		'orderby' => 'date',
		'posts_per_page' => '10',
        'post_status' => array('publish')
    ];
    
	$context['past_posts'] = Timber::get_posts( $args );
}

Timber::render( $templates, $context );
