<?php

use Timber\Timber;
use Timber\Post;

$context = Timber::get_context();

$timber_post = new Post();

$context['post'] = $timber_post;

$context['fields'] = get_fields();

function get_post_primary_category($post_id, $term='course-category', $return_all_categories=false) {
  $return = array();

  if (empty($return['primary_category']) || $return_all_categories){
	  $categories_list = get_the_terms($post_id, $term);

	  if (empty($return['primary_category']) && !empty($categories_list)){
		  $return['primary_category'] = $categories_list[0];
	  }
	  if ($return_all_categories){
		  $return['all_categories'] = array();

		  if (!empty($categories_list)){
			  foreach($categories_list as &$category){
				  $return['all_categories'][] = $category->term_id;
			  }
		  }
	  }
  }
  return $return;
}

$post_primary_cat = get_post_primary_category($timber_post->id);

// $args = [
//     'post_type' => 'testimonial',
//     'posts_per_page' => 3,
//     'order' => 'ASC',
// 	'tax_query' => [
// 		[
// 			'taxonomy' => 'course-category',
// 			'field'    => 'slug',
// 			'terms'    => $post_primary_cat['primary_category']->slug,
// 		]
// 	]
// ];

$args = [
    'post_type' => 'testimonial',
    'posts_per_page' => 3,
    'order' => 'ASC'
];

$context['testimonials'] = Timber::get_posts( $args );

Timber::render( 'single-course.twig', $context );


