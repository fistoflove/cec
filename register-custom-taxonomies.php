<?php
if ( ! function_exists( 'course_category' ) ) {

// Register Custom Taxonomy
function course_category() {

	$labels = array(
		'name'                       => _x( 'Course Categories', 'Taxonomy General Name', 'cec' ),
		'singular_name'              => _x( 'Course Category', 'Taxonomy Singular Name', 'cec' ),
		'menu_name'                  => __( 'Categories', 'cec' ),
		'all_items'                  => __( 'All Course Categories', 'cec' ),
		'parent_item'                => __( 'Parent Course Category', 'cec' ),
		'parent_item_colon'          => __( 'Parent Course Category:', 'cec' ),
		'new_item_name'              => __( 'New Course Category', 'cec' ),
		'add_new_item'               => __( 'Add New Course Category', 'cec' ),
		'edit_item'                  => __( 'Edit Course Category', 'cec' ),
		'update_item'                => __( 'Update Course Category', 'cec' ),
		'view_item'                  => __( 'View Course Category', 'cec' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'cec' ),
		'add_or_remove_items'        => __( 'Add or remove Course Categories', 'cec' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'cec' ),
		'popular_items'              => __( 'Popular Course Categories', 'cec' ),
		'search_items'               => __( 'Search Course Categories', 'cec' ),
		'not_found'                  => __( 'Not Found', 'cec' ),
		'no_terms'                   => __( 'No categories', 'cec' ),
		'items_list'                 => __( 'Course Categories list', 'cec' ),
		'items_list_navigation'      => __( 'Course Categories list navigation', 'cec' ),
	);
	$rewrite = array(
		'slug'                       => 'courses',
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'course-category', array( 'course', 'testimonial' ), $args );

}

flush_rewrite_rules();

add_action( 'init', 'course_category', 0 );

}