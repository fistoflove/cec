<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Content", "textarea"],
        ["Background", "image"],
        ["Image", "image"],
        ["Button", "clone", "group_62ddf02528621"],
        ["Category", "clone", "group_62f07d1672540"],
        ["Color", "clone", "group_62d9bc6dc3a05"],
    ]
);
