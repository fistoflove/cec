<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Items", "repeater", [
            ["Icon", "text"],
            ["Number", "number"],
            ["Label", "text"]
        ]],
    ]
);
