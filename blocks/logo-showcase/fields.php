<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Logo Strip",
    [
        ["Items", "repeater", [
            ["Logo", "image"]
        ]],
    ]
);

$fields->register_tab(
    "Button One",
    [
        ["Button", "clone", "group_62ddf02528621"],
    ]
);