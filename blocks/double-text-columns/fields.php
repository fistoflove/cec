<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Left", "wysiwyg"],
        ["Right", "wysiwyg"],
    ]
);

$fields->register_tab(
    "Heading",
    [
        ["Color", "clone", "group_62d9bc6dc3a05"]
    ]
);