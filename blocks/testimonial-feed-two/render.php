<?php

use Timber\Timber;
use Timber\Post;
use Mold\Helper\Scss;
use Mold\Helper\Fields;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

$context['compiled_css']  = Scss::compile_styles(__DIR__);

$context['local_path']  = "/wp-content/themes/cec/blocks/testimonial-feed-one/";

$args = [
    'post_type' => 'testimonial',
    'posts_per_page' => 99,
    'order' => 'ASC'
];

$context['testimonials'] = Timber::get_posts( $args );

Timber::render( 'template.twig', $context);