<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Background Image", "image"],
        ["Image", "image"],
    ]
);

$fields->register_tab(
    "Links",
    [
        ["Items", "repeater", [
            ["Link", "link"],
            ["Color", "clone", "group_62d9bc6dc3a05"],
        ]],
    ]
);