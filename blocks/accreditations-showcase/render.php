<?php

use Timber\Timber;
use Timber\Post;
use Mold\Helper\Scss;
use Mold\Helper\Fields;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

$context['compiled_css']  = Scss::compile_styles(__DIR__);

$context['block_path']  = "/wp-content/themes/cec/blocks/content-cards-one/";

$context['categories'] = get_terms( array(
    'taxonomy' => 'course-category',
    'hide_empty' => false,
));

Timber::render( 'template.twig', $context);