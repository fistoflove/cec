<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Items", "repeater", [
            ["Content", "wysiwyg"],
        ]],
    ]
);