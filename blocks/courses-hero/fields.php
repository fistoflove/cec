<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Content", "textarea"],
        ["Background", "image"],
        ["Items", "repeater", [
            ["Link", "link"],
            ["Color", "clone", "group_62d9bc6dc3a05"],
        ]],
    ]
);