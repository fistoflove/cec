<?php

use Timber\Timber;
use Timber\Post;
use Timber\Menu;
use Mold\Helper\Scss;
use Mold\Helper\Fields;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

$context['compiled_css']  = Scss::compile_styles(__DIR__);

Timber::render( 'template.twig', $context);