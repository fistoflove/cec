<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Content", "wysiwyg"],
        ["Image", "image"],
        ["Color", "clone", "group_62d9bc6dc3a05"],
    ]
);
