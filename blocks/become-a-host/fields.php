<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Content Top", "wysiwyg"],
        ["Content Bottom", "wysiwyg"],
        ["Items", "repeater", [
            ["Title", "image"],
            ["Content", "textarea"]
        ]],
    ]
);
$fields->register_tab(
    "CTA",
    [
        ["Title", "text"],
        ["Content", "wysiwyg"],
        ["Background Color", "clone", "group_62d9bc6dc3a05"],
        ["Form", "clone", "group_62f581879ba10"],
    ]
);
