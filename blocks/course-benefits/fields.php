<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Subtitle", "text"],
        ["Items", "repeater", [
            ["Title", "text"],
            ["Icon", "image"],
        ]],
    ]
);