<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Tabs",
    [
        ["Items", "repeater", [
            ["Title", "text"],
            ["Icon", "image"],
            ["Content", "wysiwyg"],
        ]],
    ]
);