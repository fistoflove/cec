<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Subtitle", "textarea"],
    ]
);

$fields->register_tab(
    "Link Boxes",
    [
        ["Items", "repeater", [
            ["Link", "link"],
            ["Content", "textarea"],
            ["Image", "image"],
            ["Color", "clone", "group_62d9bc6dc3a05"]
        ]],
    ]
);