<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Content", "wysiwyg"],
        ["Image", "image"],
        ["Background Image", "image"],
    ]
);

$fields->register_tab(
    "Button One",
    [
        ["Button", "clone", "group_62ddf02528621"],
    ]
);