<?php

use Timber\Timber;
use Timber\Post;
use Mold\Helper\Scss;
use Mold\Helper\Fields;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

$context['compiled_css']  = Scss::compile_styles(__DIR__);

$context['categories'] =  Timber::get_terms( array(
    'taxonomy' => 'course-category',
    'hide_empty' => false,
    'meta_key'  => 'course_finder_priority',
    'orderby'   => 'meta_value_num',
    'order' => 'DESC',
    'tax_query' => array(
        array(
            'key' => 'display_in_course_finder',
            'value' => '1',
            'type' => 'boolean',
            'compare' => '=',
        )
    )
));


// $categories = [];

// foreach($categories_unsorted as $category) {
//     // echo get_field('priority', $category);
//     array_push($categories, [
//         get_field('priority', $category) => $category
//     ]);
// }

// echo json_encode($categories);
$context['subcategories'] =  Timber::get_terms( array(
    'taxonomy' => 'course-category',
    'hide_empty' => false,
    'child_of' => 4,
    'meta_key'  => 'course_finder_priority',
    'orderby'   => 'meta_value_num',
    'order' => 'DESC',
));

// $context['categories'] =  $categories;

// add_filter( 'timber/twig', 'add_to_twig' );


// function add_to_twig( $twig ) {
//     $twig->addFunction( new Twig_Function( 'get_course_subcategories', 'get_course_subcategories' ) );
//     return $twig;
// }

// add_action( 'wp_ajax_nopriv_get_course_subcategories', 'get_course_subcategories' );

// add_action( 'wp_ajax_get_course_subcategories', 'get_course_subcategories' );


// function get_course_subcategories() {
//     // $context = Timber::get_context();

//     // $sub_categories = get_terms( array(
//     //     'taxonomy' => 'course-category',
//     //     'hide_empty' => true,
//     //     'parent' => $cat
//     // ) );

//     // $context['subcategories'] = $sub_categories;

//     // foreach ($sub_categories as $sub) {
//     //     Timber::render( 'select-option.twig', $context );
//     // };
//     return "hello";
// }
Timber::render( 'template.twig', $context);