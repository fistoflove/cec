<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Content", "wysiwyg"],
        ["Background Image", "image"],
    ]
);

$fields->register_tab(
    "Button One",
    [
        ["Button", "clone", "group_62ddf02528621"],
    ]
);

$fields->register_tab(
    "Button Two",
    [
        ["Button", "clone", "group_62ddf02528621"],
    ]
);