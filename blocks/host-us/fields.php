<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "CTA",
    [
        ["Title", "text"],
        ["Content", "wysiwyg"],
        ["Image", "image"],
        ["Form", "clone", "group_62f581879ba10"],
    ]
);
