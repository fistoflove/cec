<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Block One",
    [
        ["Line One", "text"],
        ["Line Two", "text"],
        ["Line Three", "text"],
        ["Image", "image"],
    ]
);
$fields->register_tab(
    "Block Two",
    [
        ["Title", "text"],
        ["Content", "textarea"],
        ["Image", "image"],
        ["Button", "clone", "group_62ddf02528621"],
    ]
);
