<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["First Youtube Video ID", "text"],
        ["Second Youtube Video ID", "text"],
    ]
);