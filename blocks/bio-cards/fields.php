<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Content", "wysiwyg"],
    ]
);

$fields->register_tab(
    "Link Boxes",
    [
        ["Items", "repeater", [
            ["Name", "text"],
            ["Job Title", "text"],
            ["Email", "link"],
            ["Content", "textarea"],
            ["Image", "image"],
            ["Color", "clone", "group_62d9bc6dc3a05"]
        ]],
    ]
);

$fields->register_tab(
    "Heading",
    [
        ["Color", "clone", "group_62d9bc6dc3a05"]
    ]
);