<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Content", "text"],
        ["Background Image", "image"],
        ["Button", "clone", "group_62ddf02528621"],
    ]
);