<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Link Boxes",
    [
        ["Items", "repeater", [
            ["Link", "link"],
            ["Image", "image"],
            ["Color", "clone", "group_62d9bc6dc3a05"]
        ]],
    ]
);