<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Content", "textarea"],
        ["Background Image", "image"],
        ["Mobile Background Image", "image"],
    ]
);