<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Items", "repeater", [
            ["Day", "text"],
            ["Activity", "text"],
            ["Icon", "image"],
            ["Background Image", "image"],
        ]],
    ]
);