<?php

use Timber\Timber;
use Timber\PostQuery;

global $wp_query;

$context = Timber::get_context();

$context['post_count'] = $wp_query->found_posts;

if (is_404()) {
    Timber::render( '404.twig', $context );
} else {
    Timber::render( 'search.twig', $context );
}

?>
