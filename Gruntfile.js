module.exports = function (grunt) {
  grunt.initConfig({
    sass: {
      dev: {
        files: {
          "static/main.css": "dev/scss/source.scss",
          "static/blog.css": "dev/scss/blog.scss",
          "static/course.css": "dev/scss/course.scss",
          "static/typography.css": "dev/scss/typography.scss",
        },
      },
    },
    csso: {
      compress: {
        options: {
          report: "gzip",
        },
        files: {
          "static/main.min.css": ["static/main.css"],
          "static/typography.min.css": ["static/typography.css"],
          "static/course.min.css": ["static/course.css"],
          "static/bundle.min.css": [
            "static/mold.css",
            "static/main.css",
            "static/typography.css"
          ]
        },
      },
    },
    watch: {
      files: ["dev/scss/*", "dev/scss/theme/*"],
      tasks: ["sass", "csso"],
    },
  });
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-csso");
  grunt.loadNpmTasks("grunt-contrib-watch");
};

